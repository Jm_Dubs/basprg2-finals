#ifndef MENU_H
#define MENU_H
#include<iostream>
#include<string>

using namespace std;

class Menu
{
public:
	Menu();
	Menu(int PlayerAct, int x, int y, string Move, int PlayerChoice);
	void Journey();

private:

	int PlayerAct;
	int x;
	int y;
	string Move;
	int PlayerDecision;
	bool IfSafeLocation = true;

};
#endif;

