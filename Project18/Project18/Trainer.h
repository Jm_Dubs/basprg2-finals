#ifndef TRAINER_H
#define TRAINER_H
#include<iostream>
#include<string>

using namespace std;

class Trainer
{
public:
	Trainer();
	Trainer(string NameOfTrainer, int TrainerCurrency, int TrainerBadges);
	void WhatIsYourName();


private:
	string NameOfTrainer;
	int TrainerCurrency;
	int TrainerBadges;
};
#endif;

