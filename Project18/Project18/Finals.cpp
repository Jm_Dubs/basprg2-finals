#include <iostream>
#include <string>
#include "Pokemon.h"
#include "Trainer.h"
#include "Menu.h"
#include <vector>
#include <time.h>

using namespace std;

void TitleScreen()
{
	cout << "===================================================================================================================" << endl;
	cout << R"(  _____   ____  _  ________ __  __  ____  _   _    _____ _____ __  __ _    _ _            _______ ____  _____  
 |  __ \ / __ \| |/ /  ____|  \/  |/ __ \| \ | |  / ____|_   _|  \/  | |  | | |        /\|__   __/ __ \|  __ \ 
 | |__) | |  | | ' /| |__  | \  / | |  | |  \| | | (___   | | | \  / | |  | | |       /  \  | | | |  | | |__) |
 |  ___/| |  | |  < |  __| | |\/| | |  | | . ` |  \___ \  | | | |\/| | |  | | |      / /\ \ | | | |  | |  _  / 
 | |    | |__| | . \| |____| |  | | |__| | |\  |  ____) |_| |_| |  | | |__| | |____ / ____ \| | | |__| | | \ \ 
 |_|     \____/|_|\_\______|_|  |_|\____/|_| \_| |_____/|_____|_|  |_|\____/|______/_/    \_\_|  \____/|_|  \_\
                                                                                                               
                                                                                                               )" << endl;
	cout << "===================================================================================================================" << endl;
	cout << "By: Jan Michael Dubouzet" << endl;
	cout << "11815372" << endl;
	cout << "===================================================================================================================" << endl;
	system("pause");
	system("cls");
}
void Introducing()
{
	Trainer* NameOfTrainer = new Trainer;
	NameOfTrainer->WhatIsYourName();
}
void Journey()
{
	Menu* PlayerAct = new Menu;
	PlayerAct->Journey();
}

int main()
{
	TitleScreen();
	Introducing();
	Journey();

	system("pause");
}
