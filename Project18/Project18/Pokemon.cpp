#include <iostream>
#include <string>
#include "Pokemon.h"
#include <vector>


Pokemon::Pokemon()
{
	this->NameOfPokemon = "";
	this->BaseHP = 0;
	this->HP = 0;
	this->Level = 0;
	this->BaseDamage = 0;
	this->EXP = 0;
	this->EXPToNextLevel = 0;
}

Pokemon::Pokemon(string NameOfPokemon, int BaseHP, int HP, int Level, int BaseDamage, int EXP, int EXPToNextLevel)
{
	this->NameOfPokemon = NameOfPokemon;
	this->BaseHP = BaseHP;
	this->HP = HP;
	this->Level = Level;
	this->BaseDamage = BaseDamage;
	this->EXP = EXP;
	this->EXPToNextLevel = EXPToNextLevel;
}

void Pokemon::DisplayPokemonStats()
{
	cout << "=============================================================" << endl;
	cout << "Pokemon " << this->NameOfPokemon << " stats" << endl;
	cout << "=============================================================" << endl;
	cout << "Level: " << this->Level << endl;
	cout << "Health Points: " << this->BaseHP << endl;
	cout << "Damage: ~" << this->BaseDamage << endl;
	cout << "EXP: " << this->EXPToNextLevel << "/" << this->EXP << endl;

}

void Pokemon::WildPokemonEncounter()
{
	vector<Pokemon*> WildPKMN;


	Pokemon* Pikachu = new Pokemon("Pikachu", 35, 35, 3, 45, 0, 0);
	WildPKMN.push_back(Pikachu);
	Pokemon* Mankey = new Pokemon("Mankey", 30, 30, 3, 50, 0, 39);
	WildPKMN.push_back(Mankey);
	Pokemon* Pidgey = new Pokemon("Pidgey", 30, 30, 3, 45, 0, 39);
	WildPKMN.push_back(Pidgey);
	Pokemon* Rattata = new Pokemon("Rattata", 30, 30, 3, 46, 0, 39);
	WildPKMN.push_back(Rattata);
	Pokemon* Meowth = new Pokemon("Meowth", 35, 35, 3, 45, 0, 39);
	WildPKMN.push_back(Meowth);
	Pokemon* Jigglypuff = new Pokemon("Jigglypuff", 40, 40, 3, 40, 0, 39);
	WildPKMN.push_back(Jigglypuff);
	Pokemon* Paras = new Pokemon("Paras", 35, 35, 3, 45, 0, 39);
	WildPKMN.push_back(Paras);
	Pokemon* Geodude = new Pokemon("Geodude", 35, 35, 3, 50, 0, 39);
	WildPKMN.push_back(Geodude);
	Pokemon* Zubat = new Pokemon("Zubat", 35, 35, 3, 45, 0, 39);
	WildPKMN.push_back(Zubat);
	Pokemon* Bellsprout = new Pokemon("Bellsprout", 30, 30, 3, 40, 0, 39);
	WildPKMN.push_back(Bellsprout);
	Pokemon* Odish = new Pokemon("Odish", 35, 35, 3, 40, 0, 39);
	WildPKMN.push_back(Odish);
	Pokemon* Spearow = new Pokemon("Spearow", 30, 30, 3, 40, 0, 39);
	WildPKMN.push_back(Spearow);
	Pokemon* Caterpie = new Pokemon("Caterpie", 30, 30, 3, 40, 0, 39);
	WildPKMN.push_back(Caterpie);
	Pokemon* Weedle = new Pokemon("Weedle", 35, 35, 3, 40, 0, 39);
	WildPKMN.push_back(Weedle);
	Pokemon* Clefairy = new Pokemon("Clefairy", 35, 35, 3, 40, 0, 39);
	WildPKMN.push_back(Clefairy);
}

void Pokemon::PokemonCenter()
{
	this->HP = this->BaseHP;
}

void Pokemon::AttackEnemy(Pokemon* Enemy)
{

}

void Pokemon::AttackPlayer(Pokemon* Player)
{

}

