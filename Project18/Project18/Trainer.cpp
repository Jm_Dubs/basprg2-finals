#include <iostream>
#include <string>
#include "Trainer.h"


Trainer::Trainer()
{
	this->NameOfTrainer = "";
	this->TrainerCurrency = 5000;
	this->TrainerBadges = 0;
}

Trainer::Trainer(string NameOfTrainer, int TrainerCurrency, int TrainerBadges)
{
	this->NameOfTrainer = NameOfTrainer;
	this->TrainerCurrency = TrainerCurrency;
	this->TrainerBadges = TrainerBadges;
}


void Trainer::WhatIsYourName()
{
	cout << "Hi Trainer Welcome to the World of Pokemon, What is your name?" << endl;
	cout << "Name: ";
	cin >> this->NameOfTrainer;
	system("cls");
	cout << "Nice to meet you " << this->NameOfTrainer << ", My name is Professor Oak." << endl;
	cout << "Here is your Trainer Card" << endl;
	system("pause");
	system("cls");

	cout << "=============================================================" << endl;
	cout << "TRAINER CARD" << endl;
	cout << "=============================================================" << endl;
	cout << "Name: " << this->NameOfTrainer << endl;
	cout << endl;
	cout << "HomeTown: Pallet Town" << endl;
	cout << endl;
	cout << "Gym Badges: " << this->TrainerBadges << endl;
	cout << endl;
	cout << "Money: " << this->TrainerCurrency << endl;
	cout << "=============================================================" << endl;
	system("pause");
	system("cls");
}


