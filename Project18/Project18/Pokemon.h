#ifndef POKEMON_H
#define POKEMON_H
#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string NameofPokemon, int BaseHP, int HP, int Level, int BaseDamage, int EXP, int EXPToNextLevel);
	void DisplayPokemonStats();
	void WildPokemonEncounter();
	void PokemonCenter();
	void AttackEnemy(Pokemon* Enemy);
	void AttackPlayer(Pokemon* Player);


private:
	string NameOfPokemon;
	int BaseHP;
	int HP;
	int Level;
	int BaseDamage;
	int EXP;
	int EXPToNextLevel;
	int PlayerAction;
	bool IfSafeLocation = true;
	int WildPokemons;
	
};
#endif;
