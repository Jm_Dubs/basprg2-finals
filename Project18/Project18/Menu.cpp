#include <iostream>
#include <string>
#include "Menu.h"
#include "Pokemon.h"
#include <time.h>
#include"Trainer.h"
#include<vector>

Menu::Menu()
{
	this->PlayerAct = 0;
	this->x = 0;
	this->y = 0;
	this->Move = "";
	this->PlayerDecision = 0;
	this->IfSafeLocation = true;
}

Menu::Menu(int PlayerAct, int x, int y, string Move, int PlayerDecision)
{
	this->PlayerAct = PlayerAct;
	this->x = x;
	this->y = y;
	this->Move = Move;
	this->PlayerDecision = PlayerDecision;
	this->IfSafeLocation = IfSafeLocation;
}

void Menu::Journey()
{
	srand(time(NULL));
	vector<Pokemon*>PokemonParty;
	int Pokeball = 0;
	int PokeballCost = 100;
	int TrainerMoney = 5000;
	int WildPKMNchoice = 0;
	int PokeMartDecision = 0;

	cout << "I have three(3) Pokeballs here." << endl;
	cout << "You can only choose one! Go on " << ", pick one!" << endl;
	cout << endl;
	cout << "1 - Bulbasaur               Grass Type               Level 5" << endl;;
	cout << "2 - Charmander              Fire Type                Level 5" << endl;
	cout << "3 - Squirtle                Water Type               Level 5" << endl;
	cout << endl;
	cout << "Input Choice: ";
	cin >> this->PlayerDecision;

	if (this->PlayerDecision == 1)
	{
		Pokemon* Bulbasaur = new Pokemon("Bulbasaur", 45, 45, 5, 49, 0, 39);
		cout << "You chose a grass type pokemon, Bulbasaur!" << endl;
		PokemonParty.push_back(Bulbasaur);
		system("pause");
		system("cls");

		cout << "Your Journey begins now Goodluck!" << endl;
		system("pause");
		system("cls");
	}
	else if (this->PlayerDecision == 2)
	{

		Pokemon* Charmander = new Pokemon("Charmander", 39, 39, 5, 52, 0, 39);
		cout << "You chose a fire type pokemon, Charmander!" << endl;
		PokemonParty.push_back(Charmander);
		system("pause");
		system("cls");
		cout << "Your Journey begins now Goodluck!" << endl;
		system("pause");
		system("cls");
	}
	else if (this->PlayerDecision == 3)
	{
		Pokemon* Squirtle = new Pokemon("Squirtle", 44, 44, 5, 48, 0, 39);
		cout << "You chose a water type pokemon, Squirtle!" << endl;
		PokemonParty.push_back(Squirtle);
		system("pause");
		system("cls");
		cout << "Your Journey begins now Goodluck!" << endl;
		system("pause");
		system("cls");
	}
	while (true)
	{
		if (IfSafeLocation == true)
		{
			cout << "What would you like to do?" << endl;
			cout << "[1] - Move          [2] - Pokemons          [3] - Pokemon Center           [4] - PokeMart" << endl;

			cin >> PlayerAct;
		}
		else if (IfSafeLocation == false)
		{
			cout << "What would you like to do?" << endl;
			cout << "[1] - Move          [2] - Pokemons " << endl;

			cin >> PlayerAct;
			int PokemonRandomEncounter = rand() % 100 + 1;

			if (PokemonRandomEncounter >= 30)
			{
				system("cls");
				cout << "A wild Pokemon appeared!" << endl;
				system("pause");
				system("cls");

				while (true)
				{
					cout << "What would you like to do?" << endl;
					cout << "[1] - Battle          [2] - Catch          [3] - Run away" << endl;
					cin >> WildPKMNchoice;

					if (WildPKMNchoice == 1)
					{

					}
					else if (WildPKMNchoice == 2)
					{
						system("cls");
						cout << "Pokeballs: " << Pokeball << endl;
						system("pause");
						system("cls");

						if (Pokeball > 0)
						{
							cout << "You threw a Pokeball..." << endl;
							Pokeball--;
							system("pause");
							system("cls");
						}
						if (Pokeball <= 0)
						{
							cout << "You have no more Pokeballs please buy at the nearest PokeMart" << endl;
							system("pause");
							system("cls");
							Pokeball = 0;
						}
					}
					else if (WildPKMNchoice == 3)
					{
						system("cls");
						cout << "You got away safely" << endl;
						system("pause");
						system("cls");
						break;
					}
				}
			}
		}

		if (PlayerAct == 1)
		{
			system("cls");
			cout << "Where do you want to move?" << endl;
			cout << "[W] - UP         [S] - DOWN         [A] - LEFT          [D] - RIGHT" << endl;
			cin >> Move;

			if (Move == "w")
			{
				y++;
				cout << "You are now at position (" << x << "," << y << ")" << endl;
			}
			else if (Move == "s")
			{
				y--;
				cout << "You are now at position (" << x << "," << y << ")" << endl;
			}
			else if (Move == "a")
			{
				x--;
				cout << "You are now at position (" << x << "," << y << ")" << endl;
			}
			else if (Move == "d")
			{
				x++;
				cout << "You are now at position (" << x << "," << y << ")" << endl;
			}
			if (y <= 2 && y >= -2 && x <= 1 && x >= -2) // Pallet Town
			{
				cout << "You are now in Pallet Town!" << endl;
				IfSafeLocation = true;
				system("pause");
				system("cls");
			}
			else if (y <= 4 || y >= 2 && x >= 1 && x <= -1 || x >= 2) // Route 1
			{
				cout << "You are now in Route 1!" << endl;
				IfSafeLocation = false;
				system("pause");
				system("cls");
			}
			else if (y >= 4 || y <= 7 && x >= 2 && x <= -2) // Viridian City
			{
				cout << "You are now in Viridian City!" << endl;
				IfSafeLocation = true;
				system("pause");
				system("cls");
			}
			else
			{
				cout << "You are now in an Unkwnown Location!" << endl;
				IfSafeLocation = false;
				system("pause");
				system("cls");
			}
		}
		if (PlayerAct == 2)
		{
			system("cls");
			for (int i = 0; i < PokemonParty.size(); i++)
			{
				cout << endl;
				PokemonParty[i]->DisplayPokemonStats();
				system("pause");
				system("cls");
			}

		}
		if (PlayerAct == 3)
		{
			system("cls");
			if (y <= 2 && y >= -2 && x <= 1 && x >= -2)
			{
				cout << "Welcome to the Pallet Town Pokemon Center!" << endl;
				system("pause");
			}
			else if (y >= 4 || y <= 7 && x >= 2 && x <= -2)
			{
				cout << "Welcome to the Viridian City Pokemon Center!" << endl;
				system("pause");
			}
			system("cls");
			cout << "Would you like to heal your Pokemon back to perfect health? (y/n)" << endl;
			cin >> Move;

			if (Move == "y")
			{
				system("cls");
				cout << "*ten ten tenenenen*" << endl;
				system("pause");
				system("cls");
				cout << "Your Pokemons are now fully healed!" << endl;
				for (int i = 0; i < PokemonParty.size(); i++)
				{
					PokemonParty[i]->PokemonCenter();
					PokemonParty[i]->DisplayPokemonStats();
				}
				system("pause");
				cout << "Please Come back again!" << endl;
			}
			else if (Move == "n")
			{
				system("cls");
				cout << "Please Come back again!!" << endl;
				system("pause");
				system("cls");
			}
		}
		if (PlayerAct == 4)
		{

			if (y <= 2 && y >= -2 && x <= 1 && x >= -2)
			{
				system("cls");
				cout << "Welcome to the Pallet Town PokeMart!" << endl;
				system("pause");
			}
			else if (y >= 4 || y <= 7 && x >= 2 && x <= -2)
			{
				system("cls");
				cout << "Welcome to the Viridian City PokeMart!" << endl;
				system("pause");
			}
			system("cls");
			cout << "Would you like to buy Pokeballs? (y/n)" << endl;
			cin >> Move;

			if (Move == "y")
			{
				system("cls");
				cout << "Money: $" << TrainerMoney << endl;
				cout << "Pokeball: $100" << endl;
				cout << "How many would you like to buy?" << endl;
				cin >> PokeMartDecision;
				TrainerMoney = TrainerMoney - (PokeballCost * PokeMartDecision);
				while (PokeMartDecision > TrainerMoney)
				{
					cout << "Oops you don't have enough money" << endl;
					TrainerMoney = TrainerMoney + (PokeballCost * PokeMartDecision);
					Pokeball = 0;
					system("pause");
					system("cls");
				}
				cout << "Money Left: $" << TrainerMoney << endl;
				
				cout << "You have " << Pokeball + PokeMartDecision << " Pokeballs in your inventory" << endl;
				system("pause");
				system("cls");
				Pokeball = Pokeball + PokeMartDecision;
				cout << "Thankyou sir! Please Come Again!" << endl;
				system("pause");
				system("cls");
			}
			else if (Move == "n")
			{
				system("cls");
				cout << "Thankyou sir! Please Come Again!!" << endl;
				system("pause");
				system("cls");
			}
		}
	}
}